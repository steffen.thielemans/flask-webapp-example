from flask import Flask, render_template, request
from flask_socketio import SocketIO

import asyncio
import eventlet
import threading, time

app = Flask(__name__)
app.secret_key = 'Change me! No, for real, change me!'
socketio = SocketIO(app, debug=True)


# Simpel async voorbeeldcode om de werking van async/await 
# en asyncio.run() binnen Flask / SocketIO te demonstreren
async def say_after(delay, data):
    await asyncio.sleep(delay)
    print(data)
    return data


@app.route('/')
def index():
    return render_template("chat.html")


@socketio.on('connect')
def socketioconnected():
    print("Socketio session connected.", request.sid)


@socketio.on('disconnect')
def socketiodisconnected():
    print("Socketio disconnected.")

@socketio.on('addToChat')
def chat(content):
    print("Chat message received: %s from %s" % (content, str(request.sid)))
    data = content + " from " + str(request.sid)
    socketio.emit('ReceiveFromChatServer', data)

#asyncio's async await werkt binnen flask maar werkt NIET met socketio  
@app.route('/api/await')
async def socketio_await():
    data = await say_after(1, 'Uitvoeren met async await ...')
    print(data)
    socketio.emit('ReceiveFromChatServer', data)
    return "Request uitgevoerd. Print naar terminal maar voert GEEN socketio emit uit. Bestande socketio sessies crashen zelfs..."

#asyncio.run() routine WERKT met flask en socketio
@app.route('/api/asyncio_run')
def socketio_asyncio_run():
    data = asyncio.run(say_after(1, 'Uitvoeren met asyncio.run() ...'))
    print(data)
    socketio.emit('ReceiveFromChatServer', data)
    return "Request uitgevoerd. Print naar terminal en voert ook socketio emit uit."


def achtergrondthread(delay):
    while True:
        socketio.sleep(delay)
        print("achtergrondthread uitgevoerd...")
        socketio.emit('ReceiveFromChatServer', "Ping van de achtergrondthread...")


# Webserver opstarten
if __name__ == "__main__":

    with threading.Lock():
        socketio.start_background_task(achtergrondthread, 10)

    socketio.run(app, host='localhost', port=8080, debug=True, use_reloader=False)